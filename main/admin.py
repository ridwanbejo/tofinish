from django.contrib import admin
from main.models import *

# Register your models here.

class UsersProfileAdmin(admin.ModelAdmin):
	search_fields = ['first_name',]

admin.site.register(UsersProfile, UsersProfileAdmin)

class UsersHistoryAdmin(admin.ModelAdmin):
	list_display = ('name', 'history_type', 'owner', 'start_date', 'end_date')
	list_filter = ('owner', 'history_type')
	search_fields = ['name',]

admin.site.register(UsersHistory, UsersHistoryAdmin)

class UsersItemAdmin(admin.ModelAdmin):
	list_display = ('name', 'item_type', 'owner', 'content')
	list_filter = ('owner', 'item_type',)
	search_fields = ['name',]
	
admin.site.register(UsersItem, UsersItemAdmin)