from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class UsersProfile(models.Model):
	MALE = 'ML'
	FEMALE = 'FM'

	GENDER_CHOICES = (
		(MALE, 'Male'),
		(FEMALE, 'Female'),
	)

	owner = models.ForeignKey(User, default=1)
	first_name = models.CharField(max_length=200)
	last_name = models.CharField(max_length=200)
	gender = models.CharField(max_length=2, choices=GENDER_CHOICES, default=MALE)
	about = models.TextField(blank=True, default="fill your description")
	interest = models.TextField(blank=True)

	def __unicode__(self):
		return self.first_name
    
	class Meta:
		ordering = ['first_name']


# class UsersHistory (Education, Career, Achievment)
class UsersHistory(models.Model):
	EDUCATION = 'ED'
	CAREER = 'CR'
	ACHIEVMENT = 'AC'

	HISTORY_CHOICES = (
		(EDUCATION, 'Education'),
		(CAREER, 'Career'),
		(ACHIEVMENT, 'Achievment'),
	)

	owner = models.ForeignKey(User, default=1)
	name = models.CharField(max_length=200)
	notes = models.TextField(blank=True, default="fill your description")
	start_date = models.DateTimeField()
	end_date = models.DateTimeField()
	history_type = models.CharField(max_length=2, choices=HISTORY_CHOICES, default=ACHIEVMENT)

	def __unicode__(self):
		return self.name
    
	class Meta:
		ordering = ['name']

# class UsersItem (Phone, Address, Website, SocialMedia)
class UsersItem(models.Model):
	PHONE = 'PH'
	ADDRESS = 'AD'
	WEBSITE = 'WB'
	SOCIALMEDIA = 'SM'

	ITEM_CHOICES = (
		(PHONE, 'Phone'),
		(ADDRESS, 'Address'),
		(SOCIALMEDIA, 'Social_Media'),
	)

	owner = models.ForeignKey(User, default=1)
	name = models.CharField(max_length=50)
	content = models.CharField(max_length=200, default="fill your content")
	item_type = models.CharField(max_length=2, choices=ITEM_CHOICES, default=PHONE)

	def __unicode__(self):
		return self.name
    
	class Meta:
		ordering = ['name']