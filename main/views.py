from django.shortcuts import render_to_response, redirect
from django.http import HttpResponse
from django.core.context_processors import csrf
from django.contrib.auth import authenticate, logout, login
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.template import RequestContext
from django.contrib.auth.models import User
from django.core import serializers

from task_project.models import *
from task_project.forms import *

import json
import subprocess

# Create your views here.

def index(request):
	return HttpResponse('Hello World :D...')

def login_form(request):
	if request.POST:
		user = authenticate(username=request.POST['username'], password=request.POST['password'])
		if user is not None:
			if user.is_active:
				login(request, user)
				request.session['username'] = request.POST['username']
				return redirect('/task_project/')
			else:	
				messages.add_message(request, messages.INFO, 'User belum terverifikasi')
		else:
			messages.add_message(request, messages.INFO, 'Username atau password Anda salah')

	c = {}
	c.update(csrf(request))
	return render_to_response('main/login.html', c, context_instance=RequestContext(request))

def login_process(request):
	user = authenticate(username=request.POST['username'], password=request.POST['password'])
	if user is not None:
		if user.is_active:
			login(request, user)
			request.session['username'] = request.POST['username']
			return redirect('/task_project/')
		else:	
			messages.add_message(request, messages.INFO, 'User belum terverifikasi')
			return redirect('/login/')
	else:
		messages.add_message(request, messages.INFO, 'Username atau password Anda salah')
		return redirect('/login/')
	
def logout_view(request):
	logout(request)
	return redirect('/login/')

def all_users(request):
	qDict = request.GET
	
	if (qDict.get('keyword') != ""):
		users = serializers.serialize("json", User.objects.all().filter(username__contains=qDict.get('keyword')))
		users = {'msg':'found several user', 'error':0, 'results':users}
	else:
		users = {'msg': 'not found', 'error':201}			
	
	resp = json.dumps(users)
	response = HttpResponse(resp, content_type="application/json")
	
	return response

def test_template(request):
	return render_to_response('main/layout.html')

def users_profile(request, id):
	# tampilkan profil
	# tampilkan riwayat 
	# tampilkan item profil
	# tampilkan proyek yang dia ikuti
	# tampilkan statistik task dia
	
	user = User.objects.get(id=id) 
	return render_to_response('main/profile.html', {'user': user})
