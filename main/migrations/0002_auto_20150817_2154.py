# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='usersitem',
            name='notes',
        ),
        migrations.AddField(
            model_name='usersitem',
            name='content',
            field=models.CharField(default=b'fill your content', max_length=200),
        ),
        migrations.AlterField(
            model_name='usersitem',
            name='name',
            field=models.CharField(max_length=50),
        ),
    ]
