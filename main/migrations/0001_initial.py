# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='UsersHistory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('notes', models.TextField(default=b'fill your description', blank=True)),
                ('start_date', models.TextField(blank=True)),
                ('end_date', models.TextField(blank=True)),
                ('history_type', models.CharField(default=b'AC', max_length=2, choices=[(b'ED', b'Education'), (b'CR', b'Career'), (b'AC', b'Achievment')])),
                ('owner', models.ForeignKey(default=1, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='UsersItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('notes', models.TextField(default=b'fill your description', blank=True)),
                ('item_type', models.CharField(default=b'PH', max_length=2, choices=[(b'PH', b'Phone'), (b'AD', b'Address'), (b'SM', b'Social_Media')])),
                ('owner', models.ForeignKey(default=1, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='UsersProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=200)),
                ('last_name', models.CharField(max_length=200)),
                ('gender', models.CharField(default=b'ML', max_length=2, choices=[(b'ML', b'Male'), (b'FM', b'Female')])),
                ('about', models.TextField(default=b'fill your description', blank=True)),
                ('interest', models.TextField(blank=True)),
                ('owner', models.ForeignKey(default=1, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['first_name'],
            },
        ),
    ]
