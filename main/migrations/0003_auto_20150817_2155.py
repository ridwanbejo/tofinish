# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_auto_20150817_2154'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usershistory',
            name='end_date',
            field=models.DateTimeField(),
        ),
        migrations.AlterField(
            model_name='usershistory',
            name='start_date',
            field=models.DateTimeField(),
        ),
    ]
