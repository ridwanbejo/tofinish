from django.contrib import admin
from task_project.models import *

# Register your models here.

class TaskProjectCategoryAdmin(admin.ModelAdmin):
	search_fields = ['name',]

admin.site.register(TaskProjectCategory, TaskProjectCategoryAdmin)

class TaskProjectAdmin(admin.ModelAdmin):
	list_display = ('name', 'task_project_category', 'owner', 'created_time')
	list_filter = ('owner', 'task_project_category')
	search_fields = ['name',]

admin.site.register(TaskProject, TaskProjectAdmin)

class TaskProjectMemberAdmin(admin.ModelAdmin):
	list_display = ('member', 'task_project', 'status', 'join_time', 'leave_time')
	list_filter = ('task_project',)
	search_fields = ['member__username',]
	
admin.site.register(TaskProjectMember, TaskProjectMemberAdmin)