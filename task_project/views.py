from django.shortcuts import render_to_response, redirect
from django.http import HttpResponse
from django.core.context_processors import csrf
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required
from django.core import serializers
from django.contrib.auth.models import User
from django.template import RequestContext

from task_project.models import *
from task_project.forms import *
from tasks.models import *

import json

# Create your views here.
login_url = '/login/'

@login_required(login_url=login_url)
def index(request):
	task_project = TaskProject.objects.all().filter(owner=request.user.id)
	return render_to_response('task_project/list.html', { 'task_project':task_project }, context_instance=RequestContext(request))

@login_required(login_url=login_url)
def add(request):
	if request.method == 'POST':
		form = TaskProjectForm(request.POST)
		if form.is_valid():
			user = User.objects.get(id=int(request.POST['owner']))
			project_category = TaskProjectCategory.objects.get(id=int(request.POST['task_project_category']))
			project = TaskProject(
				name = request.POST['name'],
				description = request.POST['description'],
				owner = user,
				task_project_category = project_category,
			)
			project.save()

			return redirect('/task_project')
	else:
		form = TaskProjectForm(initial={ 'owner':request.user.id })
	
	c = {}
	c.update(csrf(request))
	c.update({'form':form})
	return render_to_response('task_project/add.html', c, context_instance=RequestContext(request))

@login_required(login_url=login_url)
def delete(request, project_id):
	task_project = TaskProject.objects.get(id=int(project_id))
	task_project.delete()
	return redirect('/task_project')	


@login_required(login_url=login_url)
def edit(request, project_id):
	task_project = TaskProject.objects.get(id=int(project_id))
	form = TaskProjectForm(
		initial= { 
			'name' : task_project.name,
			'description' : task_project.description,
			'task_project_category' : task_project.task_project_category,
			'owner':task_project.owner.id 
		}
	)
	c = {}
	c.update(csrf(request))
	c.update({'form':form, 'task_project':task_project })
	return render_to_response('task_project/edit.html', c, context_instance=RequestContext(request))

@login_required(login_url=login_url)
def edit_process(request):
	if request.method == 'POST':
		form = TaskProjectForm(request.POST)
		if form.is_valid():
			user = User.objects.get(id=int(request.POST['owner']))
			project_category = TaskProjectCategory.objects.get(id=int(request.POST['task_project_category']))
			project = TaskProject.objects.get(id=int(request.POST['task_project_id']))
			project.name = request.POST['name']
			project.description = request.POST['description']
			project.owner = user
			project.task_project_category = project_category
			project.save()
			return redirect('/task_project')
		else:
			return redirect('/task_project/edit/'+request.POST['task_project_id'])

@login_required(login_url=login_url)
def detail(request, project_id):
	try:
		task_project = TaskProject.objects.get(id=int(project_id))
	
		if task_project.owner.id == request.user.id:
			task_groups = TaskGroup.objects.all().filter(task_project = task_project).order_by('id')[:20]
			tasks = Tasks.objects.all().filter(task_group=task_project.taskgroup_set.all())
			task_len = len(tasks)
			try:
				task_progress = {
					'success': int((len(tasks.filter(status='FN')) / float(task_len)) * 100), 
					'on_progress' : int((len(tasks.filter(status__in=('RV', 'OG'))) / float(task_len) ) * 100),
					'undone': int((len(tasks.filter(status__in=('PD', 'FL', 'PL'))) / float(task_len)) * 100),
				}
			except ZeroDivisionError:
				task_progress = {
					'success': 0, 
					'on_progress' : 0,
					'undone': 100,
				}

			c = {}
			c.update(csrf(request))
			c.update({'task_project':task_project, 'task_groups': task_groups, 'task_progress':task_progress })
			return render_to_response('task_project/detail.html', c, context_instance=RequestContext(request))
		else:
			return redirect('/task_project')
	except TaskProject.DoesNotExist:
		return HttpResponse('projects not found...')

@login_required(login_url=login_url)
def add_member(request, user_id, project_id):
	if user_id != "" and project_id != "":
		user = User.objects.get(id=int(user_id))
		project = TaskProject.objects.get(id=int(project_id))
		project_member = TaskProjectMember(
			member = user,
			task_project = project,
		)

		project_member.save()

		resp = json.dumps({'msg':'member has been added to project', 'error':0})
	else:
		resp = json.dumps({'msg':'failed add member to project', 'error':201})
		
	response = HttpResponse(resp, content_type="application/json")
	
	return response

@login_required(login_url=login_url)
def get_member_by_project(request, project_id):
	if project_id != "":
		project = TaskProject.objects.get(id=int(project_id))
		results = []
		for item in TaskProjectMember.objects.all().filter(task_project=project):
			results.append ({
				'id': item.id,
				'member': item.member.username,
				'status': item.get_status_display()
			})

		resp = json.dumps({'msg':'member has been loaded', 'error':0, 'results':results })
	else:
		resp = json.dumps({'msg':'failed to load a member', 'error':201})
		
	response = HttpResponse(resp, content_type="application/json")
	
	return response

@login_required(login_url=login_url)
def suggest_member_by_project(request, project_id):
	qDict = request.GET
	
	if (qDict.get('keyword') != ""):

		member_project = TaskProjectMember.objects.all().filter(task_project=int(project_id))
		
		members = []
		for member in member_project:
			members.append(member.member_id)

		members = str(tuple(members))
		members = members.replace(',)', ')', members.find(',)'))
		query =  'select * from auth_user where id not in ' + members + ' and username like "\045'+ qDict.get('keyword') +'\045" and is_active = 1 and is_staff = 0'
		temp_users =  User.objects.raw(query)
		results = serializers.serialize("json", temp_users)
		results = {'msg':'found several user', 'error':0, 'results':results}
	else:
		results = {'msg': 'not found', 'error':201}			
	
	resp = json.dumps(results)
	response = HttpResponse(resp, content_type="application/json")
	
	return response

@login_required(login_url=login_url)
def list_member(request, project_id):
	task_project = TaskProject.objects.get(id=int(project_id))
	c = {}
	c.update(csrf(request))
	c.update({'task_project':task_project })
	return render_to_response('task_project/member.html', c, context_instance=RequestContext(request))

@login_required(login_url=login_url)
def reject_member(request):
	member_id = request.POST['member_id']
	task_project_member = TaskProjectMember.objects.get(id=member_id)
	member = { 'member_id': member_id }

	if task_project_member:
		task_project_member.delete()
		results = { 'msg':'users has been deleted from project', 'error':0, 'results':member }
	else:
		results = { 'msg':'fail to delete users from project', 'error':201, 'resulst':member }
	
	resp = json.dumps(results)
	response = HttpResponse(resp, content_type="application/json")
	
	return response

@login_required(login_url=login_url)
def list_task_groups(request, project_id):
	try:
		task_project = TaskProject.objects.get(id=int(project_id))
		if (task_project.owner.id == request.user.id):
			task_groups = TaskGroup.objects.all().filter(task_project=task_project)
			c = {}
			c.update(csrf(request))
			c.update({'task_project':task_project, 'task_groups':task_groups })
			return render_to_response('task_project/task_groups.html', c, context_instance=RequestContext(request))
		else:
			return redirect('/task_project')
	except TaskProject.DoesNotExist:
		return HttpResponse('task groups not found...')

@login_required(login_url=login_url)
def list_tasks(request, project_id):
	"""
		menampilkan task yang akan habis mendekati deadline.
		Ada statusnya juga apakah task sudah selesai atau belum.
		task diurutkan berdasarkan waktu yang paling mendekati deadline.
	"""
	try:
		task_project = TaskProject.objects.get(id=int(project_id))
		if task_project.owner.id == request.user.id:
			c = {}
			c.update(csrf(request))
			c.update({'task_project':task_project })
			return render_to_response('task_project/tasks.html', c, context_instance=RequestContext(request))
		else:
			return redirect('/task_project')
	except TaskProject.DoesNotExist:
		return HttpResponse('tasks not found....')
		
@login_required(login_url=login_url)
def invitation(request):
	list_invitation = TaskProjectMember.objects.filter(status='PND', member=request.user.id)
	c = {}
	c.update(csrf(request))
	c.update({'list_invitation':list_invitation})
	return render_to_response('task_project/invitation.html', c, context_instance=RequestContext(request))

@login_required(login_url=login_url)
def approve_invitation(request, project_member_id):
	
	project_member = TaskProjectMember.objects.get(id=project_member_id)
	project_member.status = 'APV'
	project_member.save()

	# return HttpResponse('bejo ganteng')
	return redirect('/task_project/invitation')

@login_required
def invitation_before(request):
	list_invitation = TaskProjectMember.objects.filter(status='APV', member=request.user.id)
	c = {}
	c.update(csrf(request))
	c.update({'list_invitation':list_invitation})
	return render_to_response('task_project/invitation_before.html', c, context_instance=RequestContext(request))

@login_required(login_url=login_url)
def leave_member(request):
	member_id = request.POST['member_id']
	task_project_member = TaskProjectMember.objects.get(id=member_id)
	member = { 'member_id': member_id }

	if task_project_member:
		task_project_member.status = 'LEV'
		task_project_member.save()
		results = { 'msg':'users has been left from project', 'error':0, 'results':member }
	else:
		results = { 'msg':'fail to left from project', 'error':201, 'resulst':member }
	
	resp = json.dumps(results)
	response = HttpResponse(resp, content_type="application/json")
	
	return response

@login_required(login_url=login_url)
def reapprove_member(request):
	member_id = request.POST['member_id']
	task_project_member = TaskProjectMember.objects.get(id=member_id)
	member = { 'member_id': member_id }

	if task_project_member:
		task_project_member.status = 'APV'
		task_project_member.save()
		results = { 'msg':'users has been reapprove for project', 'error':0, 'results':member }
	else:
		results = { 'msg':'fail to reapprove users', 'error':201, 'resulst':member }
	
	resp = json.dumps(results)
	response = HttpResponse(resp, content_type="application/json")
	
	return response