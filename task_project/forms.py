from django import forms
from task_project.models import *

class TaskProjectForm(forms.Form):

	name = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control'}))
	description = forms.CharField(widget=forms.Textarea(attrs={'class' : 'form-control', 'rows': '4'}))
	task_project_category = forms.ModelChoiceField(TaskProjectCategory.objects.all(), widget=forms.Select(attrs={'class':'form-control'}))
	owner = forms.IntegerField(widget=forms.HiddenInput)
