# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task_project', '0007_auto_20150420_2254'),
    ]

    operations = [
        migrations.AlterField(
            model_name='taskprojectmember',
            name='leave_time',
            field=models.DateTimeField(),
        ),
    ]
