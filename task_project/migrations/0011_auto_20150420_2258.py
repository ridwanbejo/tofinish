# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task_project', '0010_auto_20150420_2257'),
    ]

    operations = [
        migrations.RenameField(
            model_name='taskprojectmember',
            old_name='members',
            new_name='member',
        ),
    ]
