# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('task_project', '0008_auto_20150420_2255'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='taskprojectmember',
            name='member',
        ),
        migrations.AddField(
            model_name='taskprojectmember',
            name='members',
            field=models.ForeignKey(default=1, to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='taskprojectmember',
            name='leave_time',
            field=models.DateTimeField(blank=True),
        ),
    ]
