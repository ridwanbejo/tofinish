# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('task_project', '0002_taskproject_name'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='taskproject',
            options={'ordering': ['name']},
        ),
        migrations.AlterModelOptions(
            name='taskprojectcategory',
            options={'ordering': ['name']},
        ),
        migrations.AddField(
            model_name='taskproject',
            name='created_time',
            field=models.DateTimeField(default='2015-04-20 15:09:41.145464+00:00', auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='taskproject',
            name='description',
            field=models.TextField(default=b'fill your description', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='taskproject',
            name='owner',
            field=models.ForeignKey(default=1, to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='taskproject',
            name='task_project_category',
            field=models.ForeignKey(default=1, to='task_project.TaskProjectCategory'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='taskprojectcategory',
            name='description',
            field=models.TextField(default=b'fill your description...', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='taskprojectcategory',
            name='name',
            field=models.CharField(default=b'unknown', max_length=50),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='taskproject',
            name='name',
            field=models.CharField(default=b'unknow', max_length=50),
        ),
    ]
