# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('task_project', '0003_auto_20150420_2241'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='taskprojectmember',
            options={'ordering': ['task_project']},
        ),
        migrations.AddField(
            model_name='taskprojectmember',
            name='join_time',
            field=models.DateTimeField(default='2015-04-20 15:09:41.145464+00:00', auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='taskprojectmember',
            name='leave_time',
            field=models.DateTimeField(default='2015-04-20 15:09:41.145464+00:00', auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='taskprojectmember',
            name='member',
            field=models.ForeignKey(default=1, to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='taskprojectmember',
            name='task_project',
            field=models.ForeignKey(default=1, to='task_project.TaskProject'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='taskproject',
            name='name',
            field=models.CharField(default=b'unknown', max_length=50),
        ),
    ]
