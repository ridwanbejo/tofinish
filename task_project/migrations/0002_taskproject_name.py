# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task_project', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='taskproject',
            name='name',
            field=models.CharField(default='2014-10-10', max_length=50),
            preserve_default=False,
        ),
    ]
