# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task_project', '0013_auto_20150420_2317'),
    ]

    operations = [
        migrations.AddField(
            model_name='taskprojectmember',
            name='status',
            field=models.CharField(default=b'PND', max_length=2, choices=[(b'LEV', b'Leaved'), (b'APV', b'Approved'), (b'PND', b'Pending')]),
            preserve_default=True,
        ),
    ]
