# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('task_project', '0006_auto_20150420_2252'),
    ]

    operations = [
        migrations.AlterField(
            model_name='taskprojectmember',
            name='member',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
