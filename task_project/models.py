from django.db import models
from django.contrib.auth.models import User
from django.forms import ModelForm

# Create your models here.
class TaskProjectCategory(models.Model):
	name = models.CharField(max_length=50, default="unknown")
	description = models.TextField(blank=True, default="fill your description...")
	
	def __unicode__(self):
		return self.name
    
	class Meta:
		ordering = ['name']

class TaskProject(models.Model):
	name = models.CharField(max_length=50, default="unknown")
	description = models.TextField(blank=True, default="fill your description")
	created_time = models.DateTimeField(auto_now=True)
	task_project_category = models.ForeignKey(TaskProjectCategory)
	owner = models.ForeignKey(User, default=1)

	def __unicode__(self):
		return self.name
    
	class Meta:
		ordering = ['name']

class TaskProjectMember(models.Model):
	LEAVED = 'LEV'
	APPROVED = 'APV'
	PENDING = 'PND'
	DISMISS = 'DSM'

	STATUS_CHOICES = (
		(LEAVED, 'Leaved'),
		(APPROVED, 'Approved'),
		(PENDING, 'Pending'),
		(DISMISS, 'Dismiss')
	)

	member = models.ForeignKey(User, default=1)
	task_project = models.ForeignKey(TaskProject)
	join_time = models.DateTimeField(auto_now=True)
	leave_time = models.DateTimeField(blank=True, null=True)
	status = models.CharField(max_length=2, choices=STATUS_CHOICES, default=PENDING)
	
	def __unicode__(self):
		return "%s" % (self.member.username)
    
	class Meta:
		ordering = ['task_project']