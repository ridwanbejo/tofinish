from django.shortcuts import render_to_response, redirect
from django.http import HttpResponse, QueryDict
from django.core.context_processors import csrf
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required
from django.core import serializers
from django.contrib.auth.models import User
from django.forms import ModelChoiceField

from task_project.models import *
from tasks.forms import *
from tasks.models import *
from tofinish.settings import MEDIA_ROOT

from datetime import datetime
from itertools import chain

import json
import os
import hashlib

# Create your views here.
login_url = '/login/'

@login_required(login_url=login_url)
def index(request):
	task_project = TaskProject.objects.all().filter(owner=request.user.id)
	return render_to_response('task_project/list.html', { 'task_project':task_project })

@login_required(login_url=login_url)
def add(request, group_id):
	task_group = TaskGroup.objects.get(id=int(group_id))
	if request.method == 'POST':
		form = TasksForm(request.POST)
		
		form.fields['task_group'] = forms.ModelChoiceField(TaskGroup.objects.all().filter(task_project=task_group.task_project), widget=forms.Select(attrs={'class':'form-control'}))
		
		project_member = TaskProjectMember.objects.all().filter(task_project=task_group.task_project)
		users_list = []
		for member in project_member:
			users_list.append(member.member.id)
		users_list = tuple(users_list)
		form.fields['executor'] = forms.ModelMultipleChoiceField(User.objects.all().filter(id__in=users_list), widget=forms.SelectMultiple(attrs={'class':'form-control'}))

		if form.is_valid():
			user = User.objects.get(id=int(request.POST['owner']))	
			executor_list = tuple(request.POST.getlist('executor'))

			tasks = Tasks.objects.create(
				name = request.POST['name'],
				description = request.POST['description'],
				owner = user,
				task_group = task_group,
				status = request.POST['status'],
				start_time = request.POST['start_time'],
				end_time = request.POST['end_time']
			)
			
			for person in executor_list:
				tasks.executor.add(person)

			tasks.save()

			return redirect(
				'/task_project/groups/detail/' + request.POST['task_group']
			)
	else:
		
		form = TasksForm(initial={ 'owner':request.user.id, 'task_group':task_group.id })
		form.fields['task_group'] = forms.ModelChoiceField(TaskGroup.objects.all().filter(task_project=task_group.task_project), widget=forms.Select(attrs={'class':'form-control'}))
		
		project_member = TaskProjectMember.objects.all().filter(task_project=task_group.task_project)
		users_list = []
		for member in project_member:
			users_list.append(member.member.id)
		users_list = tuple(users_list)
		form.fields['executor'] = forms.ModelMultipleChoiceField(User.objects.all().filter(id__in=users_list), widget=forms.SelectMultiple(attrs={'class':'form-control'}))
		

	c = {}
	c.update(csrf(request))
	c.update({ 'form':form, 'task_group':task_group })
	return render_to_response('tasks/add.html', c)

@login_required(login_url=login_url)
def delete(request, tasks_id):
	tasks = Tasks.objects.get(id=int(tasks_id))
	task_group_id = tasks.task_group.id
	tasks.delete()
	return redirect('/task_project/groups/detail/'+str(task_group_id))	

@login_required(login_url=login_url)
def edit(request, tasks_id):
	tasks = Tasks.objects.get(id=int(tasks_id))
	
	form = TasksForm(initial={ 
		'owner':request.user.id, 
		'task_group':tasks.task_group.id, 
		'name': tasks.name,
		'description': tasks.description,
		'status': tasks.status,
		'start_time': tasks.start_time,
		'end_time': tasks.end_time
	})

	form.fields['task_group'] = forms.ModelChoiceField(TaskGroup.objects.all().filter(task_project=tasks.task_group.task_project), widget=forms.Select(attrs={'class':'form-control'}))
	
	project_member = TaskProjectMember.objects.all().filter(task_project=tasks.task_group.task_project, status='APV')
	users_list = []
	for member in project_member:
		users_list.append(member.member.id)

	users_chosen = {}
	for member in tasks.executor.all():
		users_chosen[member.id] = True
		
	users_list = tuple(users_list)
	form.fields['executor'] = forms.ModelMultipleChoiceField(User.objects.all().filter(id__in=users_list), widget=forms.SelectMultiple(attrs={'class':'form-control'}))
	form.fields['executor'].initial = users_chosen


	c = {}
	c.update(csrf(request))
	c.update({'form':form, 'task_group':tasks.task_group, 'tasks':tasks })
	return render_to_response('tasks/edit.html', c)

@login_required(login_url=login_url)
def edit_process(request):
	if request.method == 'POST':
		form = TasksForm(request.POST)
		task_group = TaskGroup.objects.get(id=int(request.POST['task_group']))

		form.fields['task_group'] = forms.ModelChoiceField(TaskGroup.objects.all().filter(task_project=task_group.task_project), widget=forms.Select(attrs={'class':'form-control'}))
		project_member = TaskProjectMember.objects.all().filter(task_project=task_group.task_project)
		users_list = []
		for member in project_member:
			users_list.append(member.member.id)
		users_list = tuple(users_list)
		form.fields['executor'] = forms.ModelMultipleChoiceField(User.objects.all().filter(id__in=users_list), widget=forms.SelectMultiple(attrs={'class':'form-control'}))

		if form.is_valid():
			user = User.objects.get(id=int(request.POST['owner']))
			executor_list = tuple(request.POST.getlist('executor'))
			tasks = Tasks.objects.get(id=int(request.POST['tasks_id']))
			
			tasks.name = request.POST['name']
			tasks.description = request.POST['description']
			tasks.owner = user
			tasks.task_group = task_group
			tasks.status = request.POST['status']
			tasks.start_time = request.POST['start_time']
			tasks.end_time = request.POST['end_time']
			tasks.executor = User.objects.all().filter(id__in=executor_list)

			tasks.save()

			return redirect(
				'/task_project/groups/detail/' + request.POST['task_group']
			)
		else:
			return redirect('/tasks/edit/'+request.POST['tasks_id'])

@login_required(login_url=login_url)
def detail(request, tasks_id):
	task = Tasks.objects.get(id=int(tasks_id))
	discussion_form = TaskDiscussionForm(initial={'sender':request.user.id, 'task':task.id})
	attachment_form = TaskAttachmentForm(initial={'task':tasks_id, 'source':'TS'})
	comment_attachment_form = TaskAttachmentForm(initial={'task':tasks_id, 'source':'TC'})
	temp_list_discussion = TaskDiscussion.objects.all().filter(tasks=task)
	temp_list_attachment = TaskAttachment.objects.filter(tasks=tasks_id, source='TC')
	list_attachment = TaskAttachment.objects.filter(tasks=tasks_id, source='TS')

	list_discussion = sorted(
						chain(temp_list_discussion, temp_list_attachment), 
						key = lambda instance: instance.created_time)

	for i in range(0, len(list_attachment)):
		start_idx = list_attachment[i].name.find('---')
		end_idx = list_attachment[i].name.rfind('.')
		hash_str = list_attachment[i].name[start_idx:end_idx]
		file_name = list_attachment[i].name.replace(hash_str, '')
		list_attachment[i].file_name = file_name

	tasks_status = dict(Tasks.STATUS_CHOICES)
	
	c = {}
	c.update(csrf(request))
	c.update({ 
				'discussion_form':discussion_form, 
				'attachment_form':attachment_form, 
				'comment_attachment_form':comment_attachment_form, 
				'task':task, 
				'list_discussion':list_discussion,
				'list_attachment': list_attachment,
				'user_id':request.user.id, 
				'tasks_status':tasks_status 
			})

	return render_to_response('tasks/detail.html', c)

@login_required(login_url=login_url)
def add_group(request, project_id):
	task_project = TaskProject.objects.get(id=int(project_id))
	if request.method == 'POST':
		form = TaskGroupForm(request.POST)
		if form.is_valid():
			user = User.objects.get(id=int(request.POST['owner']))
			task_project = TaskProject.objects.get(id=int(request.POST['task_project']))	
			task_group = TaskGroup(
				name = request.POST['name'],
				description = request.POST['description'],
				owner = user,
				task_project = task_project,
			)
			task_group.save()

			return redirect(
				'/task_project/groups/' + request.POST['task_project']
			)
	else:
		task_project = TaskProject.objects.get(id=int(project_id))
		form = TaskGroupForm(initial={ 'owner':request.user.id, 'task_project':task_project.id })
		form.fields['task_project'] = forms.ModelChoiceField(TaskProject.objects.all().filter(owner=request.user.id), widget=forms.Select(attrs={'class':'form-control'}))
	
	c = {}
	c.update(csrf(request))
	c.update({ 'form':form, 'task_project':task_project })
	return render_to_response('tasks/add_group.html', c)

@login_required(login_url=login_url)
def edit_group(request, group_id, project_id):
	task_project = TaskProject.objects.get(id=int(project_id))
	task_group = TaskGroup.objects.get(id=int(group_id))
	form = TaskGroupForm(
		initial= { 
			'name' : task_group.name,
			'description' : task_group.description,
			'task_project' : task_group.task_project,
			'owner':task_group.owner.id 
		}
	)
	c = {}
	c.update(csrf(request))
	c.update({'form':form, 'task_project':task_project, 'task_group':task_group })
	return render_to_response('tasks/edit_group.html', c)

@login_required(login_url=login_url)
def edit_group_process(request):
	if request.method == 'POST':
		form = TaskGroupForm(request.POST)
		if form.is_valid():
			user = User.objects.get(id=int(request.POST['owner']))
			project = TaskProject.objects.get(id=int(request.POST['task_project']))
			task_group = TaskGroup.objects.get(id=int(request.POST['task_group_id']))
			task_group.name = request.POST['name']
			task_group.description = request.POST['description']
			task_group.owner = user
			task_group.task_project = project
			task_group.save()
			return redirect('/task_project/groups/'+request.POST['task_project'])
		else:
			return redirect('/task_project/groups/edit/'+request.POST['task_project'])

@login_required(login_url=login_url)
def delete_group(request, task_group_id):
	task_group = TaskGroup.objects.get(id=int(task_group_id))
	task_group.delete()
	return redirect('/task_project/groups/' + str(task_group.task_project.id))	

@login_required(login_url=login_url)
def detail_group(request, task_group_id):
	try:
		task_group = TaskGroup.objects.get(id=int(task_group_id))
		if task_group.owner.id == request.user.id:
			tasks = Tasks.objects.all().filter(task_group=task_group)
			task_summary = {
				'finished' : len(tasks.filter(status='FN')),
				'planning' : len(tasks.filter(status='PL')),
				'reviewed' : len(tasks.filter(status='RV')),
				'pending' : len(tasks.filter(status='PD')),
				'failed' : len(tasks.filter(status='FL')),
				'ongoing' : len(tasks.filter(status='OG')),
					
			}

			c = {}
			c.update(csrf(request))
			c.update({'task_group':task_group, 'tasks':tasks, 'task_summary':task_summary })
			return render_to_response('tasks/detail_group.html', c)
		else:
			return redirect('/task_project')	
	except TaskGroup.DoesNotExist:
		return HttpResponse('task group not found...')

@login_required(login_url=login_url)
def all_tasks_by_project(request, project_id):

	if project_id:

		task_group = TaskGroup.objects.all().filter(task_project=int(project_id))
		
		all_tasks = []
		for group in task_group:
			item = {}
			item['id'] = group.id
			item['name'] = group.name
			item['project_id'] = group.task_project.id
			item['description'] = group.description

			tasks = group.tasks_set.all()
			task_item = {}
			task_dict = []
			for task in tasks:
				task_item['id'] = task.id
				task_item['name'] = task.name
				task_item['status'] = task.get_status_display()
				task_item['start_time'] = task.start_time.strftime('%Y-%m-%d %H:%M')
				task_item['end_time'] = task.end_time.strftime('%Y-%m-%d %H:%M')
				task_item['start_time_js'] = task.start_time.strftime('%b %d %Y %H:%M:%S')
				task_item['end_time_js'] = task.end_time.strftime('%b %d %Y %H:%M:%S')

				temp_executor = ''
				for executor in task.executor.all():
					temp_executor += executor.username + ','

				task_item['executor'] = temp_executor

				task_dict.append(task_item)
				task_item = {}

			item['task'] = task_dict
			all_tasks.append(item)

		results = {'msg':'found several task group', 'error':0, 'results':all_tasks}
	else:
		results = {'msg': 'not found', 'error':201}			
	
	resp = json.dumps(results)
	response = HttpResponse(resp, content_type="application/json")
	
	return response

@login_required(login_url=login_url)
def my_task(request):
	tasks = Tasks.objects.filter(executor__in=(request.user.id, ))
	c = {}
	c.update(csrf(request))
	c.update({'tasks':tasks })
	return render_to_response('tasks/my_task.html', c)

@login_required(login_url=login_url)
def add_discussion_process(request):
	if request.method == 'POST':
		form = TaskDiscussionForm(request.POST)
		if form.is_valid():
			user = User.objects.get(id=int(request.POST['sender']))
			task = Tasks.objects.get(id=int(request.POST['task']))	
			task_discussion = TaskDiscussion(
				message = request.POST['message'],
				sender = user,
				tasks = task,
			)

			task_discussion.save()

			return redirect(
				'/tasks/' + request.POST['task']
			)
	else:
		return redirect(
			'/tasks/mine'
		)

@login_required(login_url=login_url)
def delete_discussion(request, task_discussion_id):
	discussion = TaskDiscussion.objects.get(id=task_discussion_id)
	task = discussion.tasks
	if (discussion.sender.id == request.user.id):
		discussion.delete()

	return redirect(
			'/tasks/' + str(task.id)
		)

@login_required(login_url=login_url)
def change_task_status(request):
	print request.POST['task_id']
	print request.POST['task_status']

	if request.POST:

		task = Tasks.objects.all().get(id=int(request.POST['task_id']))
		task.status = request.POST['task_status']
		task.save()

		results = {'msg':'update '+task.name+' status success...', 'error':0, 'result':task.get_status_display() }
	else:
		results = {'msg': 'not found', 'error':201}			
		
	resp = json.dumps(results)
	response = HttpResponse(resp, content_type="application/json")
	
	return response

@login_required(login_url=login_url)
def add_attachment_process(request):
	
	task_id = request.POST['task']
	if request.method == 'POST':
		form = TaskAttachmentForm(request.POST, request.FILES)
		if form.is_valid() and form.is_multipart():
			file = request.FILES['file']
			temp_file_name, temp_file_ext = os.path.splitext(file._get_name())
			hasher = hashlib.md5()
			hasher.update(str(task_id) + str(request.user.id) + '*tofinish*' + str(datetime.now()))
			filename = temp_file_name + "---" + hasher.hexdigest() + temp_file_ext
			path = ''
			file_path = '%s/%s' % (MEDIA_ROOT, str(path) + str(filename))
			
			# catat dulu informasi file di database
			user = User.objects.get(id=int(request.user.id))
			task = Tasks.objects.get(id=int(task_id))	
			task_attachment = TaskAttachment(
									name = filename,
									sender = user,
									tasks = task,
									source = request.POST['source'],
									file_path = file_path
								)
			task_attachment.save()
			
			# baru tulis filenya
			fd = open(file_path, 'wb')
			for chunk in file.chunks():
			    fd.write(chunk)
			fd.close()

			print "Thanks for uploading the file"
		else:
			print "Failed to upload file"
	else:
		print "no post data sent.."

	return redirect(
				'/tasks/' + str(task_id)
			)	


@login_required(login_url=login_url)
def delete_attachment_process(request):
	print request.POST['attachment_id']

	if request.POST:

		attachment = TaskAttachment.objects.get(id=int(request.POST['attachment_id']))
		os.unlink(attachment.file_path)
		attachment.delete()
		
		results = {'msg':'delete '+attachment.name+' success...', 'error':0 }
	else:
		results = {'msg': 'not found', 'error':201}			
		
	resp = json.dumps(results)
	response = HttpResponse(resp, content_type="application/json")
	
	return response

@login_required(login_url=login_url)
def delete_discussion_process(request):
	
	if request.POST:

		discussion = TaskDiscussion.objects.get(id=request.POST['task_discussion_id'])
		if (discussion.sender.id == request.user.id):
			discussion.delete()
		
		results = {'msg':'delete discussion success...', 'error':0 }
	else:
		results = {'msg': 'not found', 'error':201}			
		
	resp = json.dumps(results)
	response = HttpResponse(resp, content_type="application/json")
	
	return response