from django.forms import ModelChoiceField, ModelForm
from django.contrib.auth.models import User
from django import forms
from tasks.models import *
from task_project.models import *


class UserModelChoiceField(ModelChoiceField):
	def label_from_instance(self, obj):
		# Return a string of the format: "firstname lastname (username)"
		return "%s (%s)"%(obj.get_full_name(), obj.username)

class TasksAdminForm(ModelForm):
	owner = UserModelChoiceField(User.objects.all())
	
class TaskGroupForm(forms.Form):
	name = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control'}))
	description = forms.CharField(widget=forms.Textarea(attrs={'class' : 'form-control', 'rows': '4'}))
	task_project = forms.ModelChoiceField(TaskProject.objects.all(), widget=forms.Select(attrs={'class':'form-control'}))
	owner = forms.IntegerField(widget=forms.HiddenInput)

class TasksForm(forms.Form):
	name = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control'}))
	description = forms.CharField(widget=forms.Textarea(attrs={'class' : 'form-control', 'rows': '4'}))
	task_group = forms.ModelChoiceField(TaskGroup.objects.all(), widget=forms.Select(attrs={'class':'form-control'}))
	status = forms.ChoiceField(Tasks.STATUS_CHOICES, widget=forms.Select(attrs={'class':'form-control'}))
	owner = forms.IntegerField(widget=forms.HiddenInput)
	executor = forms.ModelMultipleChoiceField(queryset=User.objects.all(), widget=forms.SelectMultiple(attrs={'class':'form-control'}))
	start_time = forms.DateTimeField(widget=forms.DateTimeInput)
	end_time = forms.DateTimeField(widget=forms.DateTimeInput)

class TaskDiscussionForm(forms.Form):
	sender = forms.IntegerField(widget=forms.HiddenInput)
	task = forms.IntegerField(widget=forms.HiddenInput)
	message = forms.CharField(widget=forms.Textarea(attrs={'class':'form-control', 'rows':'4', 'placeholder':'Kirimkan pesan diskusi disini....'}))

class TaskAttachmentForm(forms.Form):
	# name = forms.CharField(max_length=50)
	file = forms.FileField()
	task = forms.IntegerField(widget=forms.HiddenInput)
	source = forms.CharField(widget=forms.HiddenInput)