from django.contrib import admin
from tasks.models import *
from tasks.forms import TasksAdminForm

# Register your models here.

class TaskGroupAdmin(admin.ModelAdmin):
	list_display = ('name', 'task_project', 'owner', 'created_time')
	list_filter = ('task_project', 'owner')
	search_fields = ['name', 'owner', 'task_group']
	date_hierarchy = 'created_time'

admin.site.register(TaskGroup, TaskGroupAdmin)

class TasksAdmin(admin.ModelAdmin):
	list_display = ('name', 'task_group', 'owner', 'created_time', 'status', 'start_time', 'end_time')
	list_filter = ('task_group', 'owner', 'status')
	search_fields = ['name', 'owner', 'task_group']
	date_hierarchy = 'created_time'
	form = TasksAdminForm
	
admin.site.register(Tasks, TasksAdmin)

class TaskDiscussionAdmin(admin.ModelAdmin):
	list_display = ('message', 'tasks', 'sender', 'created_time')
	list_filter = ('tasks', 'sender')
	search_fields = ['message', 'sender', 'tasks']
	date_hierarchy = 'created_time'

admin.site.register(TaskDiscussion, TaskDiscussionAdmin)
admin.site.register(TaskAttachment)
