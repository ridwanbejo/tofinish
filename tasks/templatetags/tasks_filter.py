from django import template
# from django.utils.importlib import import_module
import os

register = template.Library()

# @register.filter
# def isinst(value, class_str):
#     split = class_str.split('.')
#     return isinstance(value, getattr(import_module('.'.join(split[:-1])), split[-1])) 

@register.filter
def is_image(value):
	temp_file_name, temp_file_ext = os.path.splitext(value)
	if temp_file_ext == ".png" or temp_file_ext == ".jpg":
		return True
	return False

@register.filter
def is_video(value):
	temp_file_name, temp_file_ext = os.path.splitext(value)
	if temp_file_ext == ".mp4":
		return True
	return False

@register.filter
def is_pdf(value):
	temp_file_name, temp_file_ext = os.path.splitext(value)
	if temp_file_ext == ".pdf":
		return True
	return False