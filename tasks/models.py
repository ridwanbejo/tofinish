from django.db import models
from task_project.models import TaskProject, TaskProjectMember
from django.contrib.auth.models import User

# 2015-04-21 00:20:01.507595+00:00

class TaskGroup(models.Model):
	name = models.CharField(max_length=50, default="unknown")
	description = models.TextField(blank=True, default="fill your description")
	created_time = models.DateTimeField(auto_now=True)
	task_project = models.ForeignKey(TaskProject)
	owner = models.ForeignKey(User, default=1)

	def __unicode__(self):
		return self.name
    
	class Meta:
		ordering = ['name']

class Tasks(models.Model):
	FINISHED = 'FN'
	PLANNING = 'PL'
	REVIEWED = 'RV'
	PENDING = 'PD'
	FAILED = 'FL'
	ONGOING = 'OG'

	STATUS_CHOICES = (
		(FINISHED, 'Finished'),
		(PLANNING, 'Planning'),
		(REVIEWED, 'Reviewed'),
		(PENDING, 'Pending'),
		(FAILED, 'Failed'),
		(ONGOING, 'Ongoing')
			
	)
	
	name = models.CharField(max_length=50, default="unknown")
	description = models.TextField(blank=True, default="fill your description")
	created_time = models.DateTimeField(auto_now=True)
	task_group = models.ForeignKey(TaskGroup)
	owner = models.ForeignKey(User, default=1, related_name='owner')
	executor = models.ManyToManyField(User, default=(1,), related_name='executor')
	status = models.CharField(max_length=2, choices=STATUS_CHOICES, default=PLANNING)
	start_time = models.DateTimeField()
	end_time = models.DateTimeField()
	
	def __unicode__(self):
		return self.name
    
	class Meta:
		ordering = ['created_time']

class TaskAttachment(models.Model):
	TASK = 'TS'
	TASK_COMMENT = 'TC'

	SOURCE_CHOICES = (
		(TASK, 'Task'),
		(TASK_COMMENT, 'Task Comment'),
	)

	name = models.CharField(max_length=200, default="unknown")
	description = models.TextField(blank=True, default="fill your description")
	created_time = models.DateTimeField(auto_now=True)
	tasks = models.ForeignKey(Tasks)
	sender = models.ForeignKey(User, default=1)
	file_path = models.TextField(blank=True)
	source = models.CharField(max_length=2, choices=SOURCE_CHOICES, default=TASK)

	def __unicode__(self):
		return self.name
    
	class Meta:
		ordering = ['created_time']

class TaskDiscussion(models.Model):
	message = models.TextField(blank=True, default="fill your description")
	created_time = models.DateTimeField(auto_now=True)
	tasks = models.ForeignKey(Tasks)
	sender = models.ForeignKey(User, default=1)

	def __unicode__(self):
		return self.name
    
	class Meta:
		ordering = ['created_time']