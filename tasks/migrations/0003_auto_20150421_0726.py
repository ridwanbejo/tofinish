# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0002_auto_20150421_0720'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tasks',
            name='executor',
            field=models.ManyToManyField(default=(1,), related_name='executor', to=settings.AUTH_USER_MODEL),
        ),
    ]
