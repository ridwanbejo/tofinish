# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0004_remove_taskdiscussion_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tasks',
            name='status',
            field=models.CharField(default=b'PL', max_length=2, choices=[(b'FN', b'Finished'), (b'PL', b'Planning'), (b'RV', b'Reviewed'), (b'PD', b'Pending'), (b'FL', b'Failed'), (b'OG', b'Ongoing')]),
        ),
    ]
