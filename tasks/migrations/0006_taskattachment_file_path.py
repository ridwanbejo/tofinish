# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0005_auto_20150621_1301'),
    ]

    operations = [
        migrations.AddField(
            model_name='taskattachment',
            name='file_path',
            field=models.TextField(blank=True),
        ),
    ]
