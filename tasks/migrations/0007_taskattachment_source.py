# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0006_taskattachment_file_path'),
    ]

    operations = [
        migrations.AddField(
            model_name='taskattachment',
            name='source',
            field=models.CharField(default=b'TS', max_length=2, choices=[(b'TS', b'Task'), (b'TC', b'Task Comment')]),
        ),
    ]
