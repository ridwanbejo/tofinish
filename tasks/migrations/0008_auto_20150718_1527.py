# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0007_taskattachment_source'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='taskattachment',
            options={'ordering': ['created_time']},
        ),
        migrations.AlterModelOptions(
            name='taskdiscussion',
            options={'ordering': ['created_time']},
        ),
        migrations.AlterModelOptions(
            name='tasks',
            options={'ordering': ['created_time']},
        ),
        migrations.AlterField(
            model_name='taskattachment',
            name='name',
            field=models.CharField(default=b'unknown', max_length=200),
        ),
    ]
