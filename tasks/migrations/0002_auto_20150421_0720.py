# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('task_project', '0013_auto_20150420_2317'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('tasks', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='taskgroup',
            options={'ordering': ['name']},
        ),
        migrations.AlterModelOptions(
            name='tasks',
            options={'ordering': ['name']},
        ),
        migrations.AddField(
            model_name='taskattachment',
            name='created_time',
            field=models.DateTimeField(default='2015-04-21 00:20:01.507595+00:00', auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='taskattachment',
            name='description',
            field=models.TextField(default=b'fill your description', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='taskattachment',
            name='name',
            field=models.CharField(default=b'unknown', max_length=50),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='taskattachment',
            name='sender',
            field=models.ForeignKey(default=1, to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='taskattachment',
            name='tasks',
            field=models.ForeignKey(default=1, to='tasks.Tasks'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='taskdiscussion',
            name='created_time',
            field=models.DateTimeField(default='2015-04-21 00:20:01.507595+00:00', auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='taskdiscussion',
            name='message',
            field=models.TextField(default=b'fill your description', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='taskdiscussion',
            name='name',
            field=models.CharField(default=b'unknown', max_length=50),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='taskdiscussion',
            name='sender',
            field=models.ForeignKey(default=1, to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='taskdiscussion',
            name='tasks',
            field=models.ForeignKey(default=1, to='tasks.Tasks'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='taskgroup',
            name='created_time',
            field=models.DateTimeField(default='2015-04-21 00:20:01.507595+00:00', auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='taskgroup',
            name='description',
            field=models.TextField(default=b'fill your description', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='taskgroup',
            name='name',
            field=models.CharField(default=b'unknown', max_length=50),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='taskgroup',
            name='owner',
            field=models.ForeignKey(default=1, to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='taskgroup',
            name='task_project',
            field=models.ForeignKey(default=1, to='task_project.TaskProject'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='tasks',
            name='created_time',
            field=models.DateTimeField(default='2015-04-21 00:20:01.507595+00:00', auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='tasks',
            name='description',
            field=models.TextField(default=b'fill your description', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='tasks',
            name='end_time',
            field=models.DateTimeField(default='2015-04-21 00:20:01.507595+00:00'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='tasks',
            name='executor',
            field=models.ManyToManyField(default=1, related_name='executor', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='tasks',
            name='name',
            field=models.CharField(default=b'unknown', max_length=50),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='tasks',
            name='owner',
            field=models.ForeignKey(related_name='owner', default=1, to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='tasks',
            name='start_time',
            field=models.DateTimeField(default='2015-04-21 00:20:01.507595+00:00'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='tasks',
            name='status',
            field=models.CharField(default=b'PL', max_length=2, choices=[(b'FN', b'Finished'), (b'PL', b'Planning'), (b'RV', b'Reviewed'), (b'PD', b'Pending'), (b'FL', b'Failed')]),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='tasks',
            name='task_group',
            field=models.ForeignKey(default=1, to='tasks.TaskGroup'),
            preserve_default=False,
        ),
    ]
