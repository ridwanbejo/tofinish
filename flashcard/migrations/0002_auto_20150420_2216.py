# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('flashcard', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='flashcard',
            options={'ordering': ['created_time']},
        ),
        migrations.AlterModelOptions(
            name='flashcardcategory',
            options={'ordering': ['name']},
        ),
        migrations.AlterModelOptions(
            name='flashcarditem',
            options={'ordering': ['created_time']},
        ),
        migrations.AddField(
            model_name='flashcard',
            name='created_time',
            field=models.DateTimeField(default='2015-04-20 15:09:41.145464+00:00', auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='flashcard',
            name='description',
            field=models.TextField(default=b'unknown', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='flashcard',
            name='flashcard_category',
            field=models.ForeignKey(default=1, to='flashcard.FlashcardCategory'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='flashcard',
            name='name',
            field=models.CharField(default=b'unknown', max_length=100),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='flashcardcategory',
            name='description',
            field=models.TextField(default=b'unknown', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='flashcardcategory',
            name='name',
            field=models.CharField(default=b'unknown', max_length=100),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='flashcarditem',
            name='created_time',
            field=models.DateTimeField(default='2015-04-20 15:09:41.145464+00:00', auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='flashcarditem',
            name='description',
            field=models.TextField(default=b'unknown', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='flashcarditem',
            name='flashcard',
            field=models.ForeignKey(default=1, to='flashcard.Flashcard'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='flashcarditem',
            name='name',
            field=models.CharField(default=b'unknown', max_length=100),
            preserve_default=True,
        ),
    ]
