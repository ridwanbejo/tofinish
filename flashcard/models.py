from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class FlashcardCategory(models.Model):
	name = models.CharField(max_length=100, default="unknown")
	description = models.TextField(blank=True, default="unknown")

	def __unicode__(self):
		return self.name
    
	class Meta:
		ordering = ['name']

class Flashcard(models.Model):
	name = models.CharField(max_length=100, default="unknown")
	description = models.TextField(blank=True, default="unknown")
	created_time = models.DateTimeField(auto_now=True)
	flashcard_category = models.ForeignKey(FlashcardCategory)
	owner = models.ForeignKey(User, default=1)
	
	def __unicode__(self):
		return "%s" % (self.name)
    
	class Meta:
		ordering = ['created_time']

class FlashcardItem(models.Model):
	name = models.CharField(max_length=100, default="unknown")
	description = models.TextField(blank=True, default="unknown")
	created_time = models.DateTimeField(auto_now=True)
	flashcard = models.ForeignKey(Flashcard)
	owner = models.ForeignKey(User, default=1)
	
	def __unicode__(self):
		return "%s" % (self.name)
    
	class Meta:
		ordering = ['created_time']