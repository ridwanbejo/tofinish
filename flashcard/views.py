from django.shortcuts import render_to_response, redirect
from django.http import HttpResponse
from django.core.context_processors import csrf
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required
from django.core import serializers
from django.contrib.auth.models import User
from django.template import RequestContext

from flashcard.models import *
from flashcard.forms import *

import json

# Create your views here.
login_url = '/login/'

@login_required(login_url=login_url)
def index(request):
	flashcard = Flashcard.objects.all().filter(owner=request.user.id)
	c = {}
	c.update(csrf(request))
	c.update({ 'flashcard':flashcard })
	return render_to_response('flashcard/list.html', c, context_instance=RequestContext(request))

@login_required(login_url=login_url)
def add(request):
	if request.method == 'POST':
		form = FlashcardForm(request.POST)
		if form.is_valid():
			user = User.objects.get(id=int(request.POST['owner']))
			flashcard_category = FlashcardCategory.objects.get(id=int(request.POST['flashcard_category']))
			project = Flashcard(
				name = request.POST['name'],
				description = request.POST['description'],
				owner = user,
				flashcard_category = flashcard_category,
			)
			project.save()

			return redirect('/flashcard')
	else:
		form = FlashcardForm(initial={ 'owner':request.user.id })
	
	c = {}
	c.update(csrf(request))
	c.update({'form':form})
	return render_to_response('flashcard/add.html', c,  context_instance=RequestContext(request))

@login_required(login_url=login_url)
def delete(request, flashcard_id):
	flashcard = Flashcard.objects.get(id=int(flashcard_id))
	flashcard.delete()
	return redirect('/flashcard')	

@login_required(login_url=login_url)
def edit(request, flashcard_id):
	flashcard = Flashcard.objects.get(id=int(flashcard_id))
	form = FlashcardForm(
		initial= { 
			'name' : flashcard.name,
			'description' : flashcard.description,
			'flashcard_category' : flashcard.flashcard_category,
			'owner':flashcard.owner.id 
		}
	)
	c = {}
	c.update(csrf(request))
	c.update({'form':form, 'flashcard':flashcard })
	return render_to_response('flashcard/edit.html', c,  context_instance=RequestContext(request))

@login_required(login_url=login_url)
def edit_process(request):
	if request.method == 'POST':
		form = FlashcardForm(request.POST)
		if form.is_valid():
			user = User.objects.get(id=int(request.POST['owner']))
			flashcard_category = FlashcardCategory.objects.get(id=int(request.POST['flashcard_category']))
			flashcard = Flashcard.objects.get(id=int(request.POST['flashcard_id']))
			flashcard.name = request.POST['name']
			flashcard.description = request.POST['description']
			flashcard.owner = user
			flashcard.flashcard_category = flashcard_category
			flashcard.save()
			return redirect('/flashcard')
		else:
			return redirect('/flashcard/edit/'+request.POST['flashcard_id'])

@login_required(login_url=login_url)
def add_item(request):
	flashcard_id = request.POST['flashcard-item-flashcard']
	flashcard_item_name = request.POST['flashcard-item-name']

	user = User.objects.get(id=int(request.user.id))
	flashcard = Flashcard.objects.get(id=int(flashcard_id))
	flashcard_item = FlashcardItem(
		name = flashcard_item_name,
		description = "",
		owner = user,
		flashcard = flashcard,
	)

	flashcard_item.save()
	results = serializers.serialize("json", FlashcardItem.objects.all().filter(flashcard=flashcard))
	results = { 'msg':'flashcard item has been saved', 'error':200, 'results': results}
	
	resp = json.dumps(results)
	response = HttpResponse(resp, content_type="application/json")
	
	return response

@login_required(login_url=login_url)
def delete_item(request):
	flashcard_item_id = request.POST['flashcard-item-id']
	flashcard_item = FlashcardItem.objects.get(id=int(flashcard_item_id))
	
	flashcard_item_id = { 'flashcard_item_id': flashcard_item_id }

	if flashcard_item:
		flashcard_item.delete()
		results = { 'msg':'flashcard item has been deleted', 'error':200, 'results':flashcard_item_id }
	else:
		results = { 'msg':'fail to delete flashcard_item', 'error':201, 'resulst':flashcard_item_id }
	
	resp = json.dumps(results)
	response = HttpResponse(resp, content_type="application/json")
	
	return response