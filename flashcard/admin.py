from django.contrib import admin
from flashcard.models import *

# Register your models here.
admin.site.register(FlashcardCategory)
admin.site.register(Flashcard)
admin.site.register(FlashcardItem)
