from django import forms
from flashcard.models import *

class FlashcardForm(forms.Form):

	name = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control'}))
	description = forms.CharField(widget=forms.Textarea(attrs={'class' : 'form-control', 'rows': '4'}))
	flashcard_category = forms.ModelChoiceField(FlashcardCategory.objects.all(), widget=forms.Select(attrs={'class':'form-control'}))
	owner = forms.IntegerField(widget=forms.HiddenInput)