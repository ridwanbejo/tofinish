from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    # Examples:
    # url(r'^$', 'tofinish.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    # url(r'^grappelli/', include('grappelli.urls')),
    url(r'^index/', 'main.views.index'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'test_template/$', 'main.views.test_template'),
    url(r'login/$', 'main.views.login_form'),
    url(r'login/process/$', 'main.views.login_process'),
    url(r'logout/', 'main.views.logout_view'),
    url(r'^profile/(\d+)/$', 'main.views.users_profile'),

    url(r'task_project/$', 'task_project.views.index'),
 	url(r'task_project/add$', 'task_project.views.add'),
 	url(r'^task_project/(\d+)', 'task_project.views.detail'),
 	url(r'^task_project/delete/(\d+)', 'task_project.views.delete'),   
 	url(r'^task_project/edit/(\d+)', 'task_project.views.edit'),   
 	url(r'^task_project/edit_process$', 'task_project.views.edit_process'),   
 	url(r'^task_project/member/(\d+)', 'task_project.views.list_member'),
 	url(r'^task_project/groups/(\d+)', 'task_project.views.list_task_groups'),
 	url(r'^task_project/tasks/(\d+)', 'task_project.views.list_tasks'),
 	url(r'^task_project/groups/edit/(\d+)/(\d+)', 'tasks.views.edit_group'),   
 	url(r'^task_project/groups/delete/(\d+)', 'tasks.views.delete_group'),   
 	url(r'^task_project/groups/edit_process$', 'tasks.views.edit_group_process'),   
 	url(r'^task_project/groups/add/(\d+)', 'tasks.views.add_group'),
 	url(r'^task_project/groups/detail/(\d+)', 'tasks.views.detail_group'),
 	url(r'^task_project/invitation/$', 'task_project.views.invitation'),
    url(r'^task_project/invitation/before/$', 'task_project.views.invitation_before'),
    url(r'^task_project/invitation/approve/(\d+)', 'task_project.views.approve_invitation'),
    
 	url(r'^tasks/add/(\d+)', 'tasks.views.add'),
 	url(r'^tasks/edit/(\d+)', 'tasks.views.edit'),
 	url(r'^tasks/edit_process', 'tasks.views.edit_process'),
 	url(r'^tasks/delete/(\d+)', 'tasks.views.delete'),   
 	url(r'^tasks/(\d+)', 'tasks.views.detail'),
 	url(r'^tasks/mine', 'tasks.views.my_task'),
 	url(r'^tasks/discussion/add', 'tasks.views.add_discussion_process'),
    url(r'^tasks/discussion/delete/(\d+)', 'tasks.views.delete_discussion'),
    url(r'^tasks/attachment/add', 'tasks.views.add_attachment_process'),
    
    url(r'^flashcard/$', 'flashcard.views.index'),
 	url(r'^flashcard/add$', 'flashcard.views.add'),
 	url(r'^flashcard/delete/(\d+)', 'flashcard.views.delete'), 
 	url(r'^flashcard/edit/(\d+)', 'flashcard.views.edit'),   
 	url(r'^flashcard/edit_process$', 'flashcard.views.edit_process'),  

 	url(r'^api/users/all$', 'main.views.all_users'),   
 	url(r'^api/task_project_member/add/(\d+)/(\d+)', 'task_project.views.add_member'),   
 	url(r'^api/task_project_member/by_project/(\d+)', 'task_project.views.get_member_by_project'),   
 	url(r'^api/task_project_member/suggest/(\d+)', 'task_project.views.suggest_member_by_project'),   
 	url(r'^api/task_project_member/reject/', 'task_project.views.reject_member'), 
    url(r'^api/task_project_member/leave/', 'task_project.views.leave_member'),
    url(r'^api/task_project_member/reapprove/', 'task_project.views.reapprove_member'),
    url(r'^api/tasks/task_project/(\d+)', 'tasks.views.all_tasks_by_project'),
    url(r'^api/tasks/change/', 'tasks.views.change_task_status'),
    url(r'^api/tasks/attachment/delete/', 'tasks.views.delete_attachment_process'),
    url(r'^api/tasks/discussion/delete/', 'tasks.views.delete_discussion_process'),
    url(r'^api/flashcard/item/add/', 'flashcard.views.add_item'),   
	url(r'^api/flashcard/item/delete/', 'flashcard.views.delete_item'),   
		 										
]  + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

